var path = require('path');
global.appRoot = path.resolve(__dirname);

const routes = require("./api/routes/route.js")
var express = require('express');
//Load HTTP module
const http = require("http");
const hostname = '127.0.0.1';
const port = 3000;

var app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');

const cors = require('cors');
app.use(cors());

const mongoose = require('mongoose');

main().catch(err => console.log(err));


async function main() {
  await mongoose.connect('mongodb://localhost:27017/test');
  
  // use `await mongoose.connect('mongodb://user:password@localhost:27017/test');` if your database has auth enabled
}

app.use(express.urlencoded());
app.use(express.json());

var httpServer = http.createServer(app);

httpServer.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
  console.log(`Root directory is located ${global.appRoot}`);
});

app.use('/', routes);

