// Controller du carnet d'adresse

const { default: mongoose } = require("mongoose");
const userSchema = require("../schema/userSchema");

User = mongoose.model('User');

// Sensé renvoyer la vue du carnet d'adresse avec toute les fiches contact non triées
exports.getCarnetAdresseGetAll = (req, res) => {
    User.find({}, (err, User) => {
        if(err) {
            res.status(500)
            console.log(err);
            return res.json({message: 'Erreur serveur.'})
        }
        console.log(User);
        res.status(200);
        console.log(User);
        res.json(User)
    })
}

// Sensé renvoyer la vue du carnet d'adresse avec toute les fiches de contact filtrées/triées
exports.postCarnetAdresseFiltered = (req, res) => {

    let newUser = new User(req.body);

    newUser.save((err, user) => {
        if(err) {
            res.status(400);
            console.log(err);
            return res.json({message: 'Erreur serveur.'})
        }
        res.status(201);
        return res.json({message: 'saved.'})
    });
};

// Recherche de contact par le lastName
exports.findContactByLastName = (req, res) => {

    User.find({lastName: req.params.lastName}, (err, User) => {
        if(err) {
            res.status(500)
            console.log(err);
            return res.json({message: 'Erreur serveur.'})
        }
        res.status(200);
        res.json(User)
    })
};

deleteContact = (req, res) => {
    
}