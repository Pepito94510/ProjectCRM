// getting-started.js
const mongoose = require('mongoose');

const Schema = mongoose.Schema;



let userSchema = new Schema({
      lastName : {
        type : String,
        required : true
      },
      firstName : {
        type : String,
        required : true
      },
      email : {
        type : String,
        required : true,
        unique : true
      },
      mobile : {
        type : String,
        required : false
      }
    });

module.exports = mongoose.model('User', userSchema);