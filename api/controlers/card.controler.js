// Controler des cards (fiche de contact)

// Sensé renvoyer la vue de consultation de la fiche contact
exports.getShowCardWithId = (req, res) => {
    res.send(`NOT IMPLEMENTED Hello fiche contact id ${req.params.id}`);
    console.log(`Accessing ${req.path}`);
};

// Sensé renvoyer la vue de modification de la fiche contact avec les champs préremplis
exports.getModifyCardWithId = (req, res) => {
    res.send(`NOT IMPLEMENTED Hello modif fiche contact id ${req.params.id}`);
    console.log(`Accessing ${req.path}`);
};

// Sensé recevoir un formulaire pour mettre a jour une fiche de contact
// Devrait renvoyer un code OK une fois fait puis redirect sur la consultation
exports.postModifyCardWithId = (req, res) => {
    res.send(`NOT IMPLEMENTED Hello update fiche contact id ${req.params.id}`);
    console.log(`Accessing ${req.path}`);
};

// Sensé supprimer une fiche de contact via son ID
exports.deleteModifyCardWithId = (req, res) => {
    res.send(`NOT IMPLEMENTED Hello delete fiche contact id ${req.params.id}`);
    console.log(`Accessing ${req.path}`);
}

// Sensé renvoyer une vue de création de fiche de contact
exports.getCreaCard = (req, res) => {
    res.sendFile(`${global.appRoot}/api/vues/createCard.vue.html`);
    console.log(`Accessing ${req.path} using ${req.method}`);
};

// Sensé enregistrer une formaulaire de contact en base 
exports.postCreateCard = (req, res) => {
    res.send(`NOT IMPLEMENTED Hello createing fiche contact`);
    console.log(`Accessing ${req.path} using ${req.method}`);
}