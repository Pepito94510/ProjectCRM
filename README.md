# Le projet 
C'est un CRM dans le cadre de la semaine Bootcamp au sein de l'école MyDigitalSchool.
L'appli se conmpose simplement d'un carnet d'adresse, auquel on doit pouvoir ajouter une nouvelle fiche contact, puis consulter, modifier, ou supprimer cette fiche contact.

Si le temps est suffisant sur le bootcamp, il faudrait faire une petite interface graphique, et peut-être un système de tris sur les role, entreprise, etc. des contacts dans le carnet d'adresse.

## Instalation
Dans une invite de commande (cmd, powershel, gti bash...), tapez les commandes suivantes:
```
git clone https://gitlab.com/Pepito94510/ProjectCRM.git
cd ProjectCRM
npm install
```

Pensez bien en cas de developpement d'une nouvelle feature, faire une branch avec le nom de la feature ou numéro du ticket.

## Lancement du projet 

Pour lancer le projet, après l'avoir installer: 
```
npm start
```
Le server de dev redémarera automatiquement à chaque modification d'un fichier.

A chaque création d'un endpoint, l'ajouter dans doc/api.json avec son status (TODO, KO, OK)

## Auteurs
[@Pepito94510](https://gitlab.com/Pepito94510), [@MattTour](https://gitlab.com/MattTour), [@iti98](https://gitlab.com/iti98)

# TODOs
- [x] Route concernant le carnet d'adresse (./api/routes/roue.js)
- [x] Route concernant les entrés du carnet d'adresse (./api/routes/route.js)
- [ ] Controlers pour le carnet d'adresse
- [ ] Controlers pour les entrés du carnet d'adresse 
- [x] Définition des routes 
- [ ] Créer la vue de l'adresse
- [ ] Créer la vue d'une entrée du carnet d'adresse
- [ ] Créer la vue de l'ajout d'une entré au carnet d'adresse
  - [ ] Créer la vue de modification d'une entré du carnet d'adresse


## Helps

[Cette doc sur MDN WD](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)

[Excellente aide](https://ourcodeworld.com/articles/read/261/how-to-create-an-http-server-with-express-in-node-js)

[Un bon vieux Hello World!, rien de mieux pour comprendre](https://expressjs.com/fr/starter/hello-world.html)