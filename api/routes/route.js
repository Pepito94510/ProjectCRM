var express = require('express');

const router = express.Router();

const carnetControler = require('../controlers/carnetAdresse.controler.js');
const cardControler = require('../controlers/card.controler.js');


// Route concernant la page principale du carnet d'adresse (Affichage de l'ensemble des Contacts)
router.route('/carnet_adresse')
  .get(carnetControler.getCarnetAdresseGetAll)

// Creation de Contact
router.route("/create_entry")
  // .get(cardControler.getCreaCard)
  .post(carnetControler.postCarnetAdresseFiltered);

// Recherche de Contact
router.route("/findByLastName/:lastName")
  .get(carnetControler.findContactByLastName);






// Route concernant une entré du carnet d'adresse, e.g.: http://127.0.0.1:3000/show_entry/98
router.route('/show_entry/:id')
  .get(cardControler.getShowCardWithId); 

// Pour l'instant tu peux test en tapant sur http://127.0.0.1:3000/modify_entry/45 par exemple (le nombre que tu veux)
router.route("/modify_entry/:id")
  .get(cardControler.getModifyCardWithId)
  .post(cardControler.postModifyCardWithId)
  .delete(cardControler.deleteModifyCardWithId);




// Je suis fier de moi j'ai jamais fais de node avec express, en fait c'est plutot simple 

// Renvoyer la racine du site vers la page carnet_adresse
// Je me suis dis que c'était la page principale mais si tu veux mettre une autre en mode Welcome to CRM, n'hésites pas
router.route('/')
  .get((req, res) => {
    res.redirect('/carnet_adresse')
  });

// Permet de link des fichier css, js, png, et tout autres dans le dossier public a la racine du projet
router.use(express.static(`${global.appRoot}/public`));

// Gestion rapide du 404
router.use(function(req, res, next) {
  res.status(404).send("Sorry, that route doesn't exist. Have a nice day :)");
});

/**Je crois qu'il faut bien mettre toute les routes puis la redirection de l'index puis la gestion de la 404, 
 * sinon ça risque de pas prendre les routes en compte
 * */

module.exports = router;